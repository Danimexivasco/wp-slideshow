var loaded = false;
var posts = [];
var iPost = -1;
var changes;
var domain = "";
var lastCorrectDomain = "";

document.addEventListener("DOMContentLoaded", function () {
    if (!loaded) {
        // console.info('Loading...');
        document.querySelector('#viewingPosts').style.display = 'none';
        document.querySelector('.sk-chase').style.display = 'block';
    }
    // console.log('Loaded');
    fetchData();
    document.querySelector('.slides').style.display = 'none';
    document.querySelector('#errorMsg').style.display = 'none';
    document.onkeydown = checkKey;

    // Adding the eventListeners to click and touch events
    document.querySelector('#left').addEventListener('click', previousPost);
    document.querySelector('#right').addEventListener('click', nextPost);
    document.querySelector('.slides').addEventListener('click', goToPost);
    document.querySelector('.info').addEventListener('click', preventNewTab);
    document.querySelector('.slides').addEventListener('touchstart', handleTouchStart, false);
    document.querySelector('.slides').addEventListener('touchmove', handleTouchMove, false);
});


const fetchData = async (domn) => {
    domain = ((typeof domn !== 'undefined') && (domn !== '')) ? domn : 'wptavern.com';
    try {
        document.querySelector('.slides').style.display = 'none';
        document.querySelector('#viewingPosts').style.display = 'none';
        document.querySelector('.sk-chase').style.display = 'block';
        const data = await axios.get(`https://${domain}/wp-json/wp/v2/posts`);
        posts = data.data;
        loaded = true;
        lastCorrectDomain = domain;
        document.querySelector('#showDomain').innerHTML = `${lastCorrectDomain}`;
        document.querySelector('#errorMsg').style.display = 'none';
        stopInterval();
        iPost = -1;
        executeInterval();
    } catch (error) {
        console.error(error);
        document.querySelector('#errorMsg').style.display = 'block';
    }
}

const intervalActions = () => {
    (iPost >= posts.length - 1) || (iPost < 0) ? iPost = 0 : iPost++;
    // (iPost >= posts.length - 1) ? iPost = 0 : (iPost === 0) ? iPost = 0 : iPost;
    document.querySelector('.sk-chase').style.display = 'none';
    document.querySelector('.slides').style.display = 'flex';
    document.querySelector('#viewingPosts').style.display = 'block';
    setData();
}

const executeInterval = () => {
    changes = setInterval(intervalActions, 5000);
}

const stopInterval = () => {
    clearInterval(changes);
}

// FN to check which key is pressing
const checkKey = (e) => {
    e = e || window.event;
    if (e.keyCode == '37') {
        previousPost();
    }
    else if (e.keyCode == '39') {
        nextPost();
    }
}


const stopEvents = () => {
    // if (!e) var e = window.event;
    // e.cancelBubble = true;
    // if (e.stopPropagation) e.stopPropagation();
    window.event.stopPropagation(); 
}

const previousPost = () => {
    iPost > posts.length - 1 ? iPost = 0 : (iPost <= 0) ? iPost = posts.length - 1 : iPost--;
    setData();
    stopEvents();

    // Restart the interval timeout
    stopInterval();
    executeInterval();
}

const nextPost = () => {
    (iPost >= posts.length - 1) ? iPost = 0 : iPost++;
    setData();
    stopEvents();

    // Restart the interval timeout
    stopInterval();
    executeInterval();
}

const goToPost = () => {
   window.open(`${posts[iPost].link}`, '_blank');
}

const setData = () => {
    if(posts.length !== 0){
        var completeDate = posts[iPost].date;
        var dissectedTime = completeDate.split('T');
        document.querySelector('.slides').style.backgroundImage = `url(${posts[iPost].jetpack_featured_media_url})`;
        document.querySelector('.slides h2 a').innerHTML = `${posts[iPost].title.rendered}`;
        document.querySelector('.date').innerHTML = `${dissectedTime[0]}`;
        document.querySelector('.slides h2 a').setAttribute('href', `${posts[iPost].link}`);
    }
}

const preventNewTab = () => {
    stopEvents();
}

//  ** FINGER SWIPE ** 
var xDown = null;                                                        
var yDown = null;

function getTouches(evt) {
  return evt.touches;   // browser API
}                                                     

function handleTouchStart(evt) {
    const firstTouch = getTouches(evt)[0];                                      
    xDown = firstTouch.clientX;                                      
    yDown = firstTouch.clientY;                                      
};                                                

function handleTouchMove(evt) {
    if ( ! xDown || ! yDown ) {
        return;
    }

    var xUp = evt.touches[0].clientX;                                    
    var yUp = evt.touches[0].clientY;

    var xDiff = xDown - xUp;
    var yDiff = yDown - yUp;

    if ( Math.abs( xDiff ) > Math.abs( yDiff ) ) {/*most significant*/
        if ( xDiff > 0 ) {
            /* left swipe */ 
            nextPost();
        } else {
            /* right swipe */
            previousPost();
        }                       
    } else {
        if ( yDiff > 0 ) {
            /* up swipe */ 
        } else { 
            /* down swipe */
        }                                                                 
    }
    /* reset values */
    xDown = null;
    yDown = null;                                             
};